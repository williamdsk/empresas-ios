//
//  EnterprisesTests.swift
//  EnterprisesTests
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import XCTest
@testable import Enterprises

/*
 Apenas para não passar em branco a parte de testes, fiz 2 pequenos testes para a classe Validation Helper
 */
class ValidationHelperTests: XCTestCase {

    var sut: ValidationHelper!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        sut = nil
    }

    func test_emailValidationHelper_mustReturnTrue_whenEmailTestedIsValid() throws {
        let testedEmail = "william.kuhs@gmail.com"
        
        let result = ValidationHelper.validateEmail(testedEmail)
        
        XCTAssertTrue(result)
    }
    
    // De acordo com o figma, existe uma verificação de email, onde após o 'domínio.' deve-se retornar email inválido utilizando 2 characteres, então assumo como padrão 3 caracteres
    func test_emailValidationHelper_mustReturnFalse_whenEmailTestedIsInvalid() throws {
        let testedEmail = "william.kuhs@gmail.cm"
        
        let result = ValidationHelper.validateEmail(testedEmail)
        
        XCTAssertFalse(result)
    }
}
