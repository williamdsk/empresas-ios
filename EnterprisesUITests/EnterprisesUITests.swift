//
//  EnterprisesUITests.swift
//  EnterprisesUITests
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import XCTest

/*
 O ideal seria utilizar textos do arquivo de strings ou localizable, porém não imlpementei tais arquivos
 sendo assim, coloquei os nomes hardcoded
 Utilizei também 1 timeout de 1 segundo no início dos testes, pois há uma transição na tela de início, e também temos o teclado que aparece ao clicar nos textfields
 */
class EnterprisesUITests: XCTestCase {
    var app: XCUIApplication!
       
    override func setUpWithError() throws {
        app = XCUIApplication()
        app.launch()
        
        continueAfterFailure = false
        
        try super.setUpWithError()
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        app = nil
    }

    func test_shouldShowEmailTextField_whenLoginScreenAppear() throws  {
        //Verifica se o textfield existe em tela
        XCTAssertTrue(XCUIApplication().textFields["Email"].waitForExistence(timeout: 1))
    }
    
    func test_shouldShowsEmailLabel_whenUserTapsInEmailTextField() throws {
        //Verifica se o textfield existe em tela
        XCTAssertTrue(XCUIApplication().textFields["Email"].waitForExistence(timeout: 1))
        
        //Recupera e pressiona o textfield de email
        XCUIApplication().textFields["Email"].tap()
        
        //Verifica se a label de email existe em tela
        XCTAssertTrue(XCUIApplication().staticTexts["Email"].exists)
    }
    
    func test_shouldShowValidationLabels_whenUserTapEnterButton_withAnIncorrectEmail() throws {
        //Verifica se o textfield existe em tela
        XCTAssertTrue(XCUIApplication().textFields["Email"].waitForExistence(timeout: 1))

        //Recupera e pressiona o textfield de email
        let emailTextField = XCUIApplication().textFields["Email"]
        emailTextField.tap()
        
        //Verifica se o teclado apareceu em tela em no máximo 1 segundo
        XCTAssertTrue(app.keyboards.element.waitForExistence(timeout: 1))

        //Recupera e pressiona o botão entrar
        let enterButton = XCUIApplication().buttons["Entrar"]
        enterButton.tap()
        
        //Verifica se as labels de validação apareceram em tela
        XCTAssertTrue(XCUIApplication().staticTexts["Email"].exists)
        XCTAssertTrue(XCUIApplication().staticTexts["Endereço de email inválido"].exists)
        
    }
}
