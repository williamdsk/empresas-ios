//
//  Color.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import Foundation
import UIKit

struct Color {
    
    static let lightNavBarTitle = UIColor(named: "light-nav-bar-title")!
    static let darkNavBarTitle = UIColor(named: "dark-nav-bar-title")!
    static let lightText = UIColor(named: "light-text")!
    static let darkText = UIColor(named: "dark-text")!
    static let lightButtonTint = UIColor.white
    static let darkButtonTint = UIColor.purple
    static let searchFieldBorder = UIColor.purple
}
