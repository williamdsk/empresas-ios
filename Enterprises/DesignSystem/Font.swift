//
//  Font.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import UIKit

struct Font {
    static let navigationTitle = UIFont.systemFont(ofSize: 22, weight: .semibold)
    static let navigationSubtitle = UIFont.systemFont(ofSize: 19, weight: .regular)
}
