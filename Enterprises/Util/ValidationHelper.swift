//
//  ValidationHelper.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import Foundation

struct ValidationHelper {
    
    static func validateEmail(_ email: String) -> Bool {        
        let pattern = #"\b[\w\.-]+@[\w\.-]+\.\w{3}\b"#
        
        let result = email.range(
            of: pattern,
            options: .regularExpression
        )

        return (result != nil)
    }
    
}
