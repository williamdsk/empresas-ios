//
//  LoginViewController.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 08/04/22.
//

import UIKit

enum State {
    case loading
    case presenting
}
class LoginViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var welcomeStackView: UIStackView!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var invalidEmailLabel: UILabel!
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordVisibilityButton: UIButton!
    @IBOutlet weak var autenticateButton: UIButton!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ioasysVectorImageView: UIImageView!
    
    //MARK: - Global Variables
    var viewModel: LoginViewModel!
    
    var state: State = .loading{
        didSet {
            switch state {
            case .loading:
                self.startLoading()
            case .presenting:
                self.startPresenting()
            }
        }
    }
    
    //MARK: - Initializer
    init(viewModel: LoginViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
        addKeyboardObservers()
        setupDelegates()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.ioasysVectorImageView.isHidden = false
            self.formView.isHidden = false
            self.welcomeStackView.isHidden = false
        }
    }
    
    //MARK: - UI Configuration
    private func configUI() {
        emailStackView.layer.borderColor = UIColor.gray.cgColor
        emailStackView.layer.borderWidth = 1
        emailStackView.layer.cornerRadius = 10
        
        passwordStackView.layer.borderColor = UIColor.gray.cgColor
        passwordStackView.layer.borderWidth = 1
        passwordStackView.layer.cornerRadius = 10
        
        passwordVisibilityButton.setTitle("", for: .normal)
        
        normalizeEmailField()
        normalizePasswordField()
    }
    
    //MARK: - Public Functions
    func setupDelegates() {
        viewModel?.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    // MARK: - Private Functions
    // Agradecimento ao Axel Kee
    // Post https://fluffy.es/move-view-when-keyboard-is-shown/
    @objc private func keyboardWillShow(
        _ notification: NSNotification
    ) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        self.view.frame.origin.y = 0 - keyboardSize.height
    }
    
    @objc private func keyboardWillHide(
        _ notification: NSNotification
    ) {
        self.view.frame.origin.y = 0
    }
    
    //MARK: - Button Actions
    @IBAction private func passwordVisibilityButtonPressed(_ sender: UIButton) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        
        let image = passwordTextField.isSecureTextEntry ? UIImage(systemName: "eye.slash") : UIImage(systemName: "eye")
        
        passwordVisibilityButton.setImage(image, for: .normal)
        passwordVisibilityButton.setPreferredSymbolConfiguration(.init(scale: .medium), forImageIn: .normal)
    }
    
    @IBAction private func autenticateButtonPressed(_ sender: UIButton) {
        var proceedToAuthentication = true
        
        var emailToAuthenticate = ""
        var passwordToAuthenticate = ""
        
        if let email = emailTextField.text, !emailTextField.text!.isEmpty, viewModel.validateEmail(email) {
            emailToAuthenticate = email
        } else {
            showEmailValidation()
            proceedToAuthentication = false
        }
        
        if let password = passwordTextField.text, !passwordTextField.text!.isEmpty {
            passwordToAuthenticate = password
        } else {
            showPasswordValidation()
            proceedToAuthentication = false
        }
        
        if proceedToAuthentication {
            let login = Login(email: emailToAuthenticate, password: passwordToAuthenticate)
            state = .loading
            viewModel?.autenticate(login: login)
        }
    }
}

// MARK: - Setup Keyboard Observers
extension LoginViewController {
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
}

// MARK: - UI Validation Handlers
extension LoginViewController {
    func normalizeEmailField() {
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes:[NSAttributedString.Key.foregroundColor: Color.darkText])
        emailLabel.textColor = .black
        invalidEmailLabel.textColor = .clear
        emailStackView.layer.borderColor = UIColor.gray.cgColor
    }
    
    func normalizePasswordField() {
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Senha", attributes:[NSAttributedString.Key.foregroundColor: Color.darkText])
        passwordLabel.textColor = .black
        passwordStackView.layer.borderColor = UIColor.gray.cgColor
    }
    
    func showEmailValidation() {
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
        
        emailLabel.textColor = .red
        invalidEmailLabel.textColor = .red
    }
    
    func showPasswordValidation() {
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Senha", attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
        passwordLabel.textColor = .red
        passwordStackView.layer.borderColor = UIColor.red.cgColor
    }
    
    func resetValidations() {
        emailLabel.textColor = .clear
        invalidEmailLabel.textColor = .clear
        passwordLabel.textColor = .clear
        emailStackView.layer.borderColor = UIColor.gray.cgColor
        passwordStackView.layer.borderColor = UIColor.gray.cgColor
    }
}

//MARK: - LoadingActivityIndicator Handler
extension LoginViewController {
    func startLoading() {
        loadingActivityIndicator.startAnimating()
        loadingView.isHidden = false
    }
    
    func startPresenting() {
        loadingActivityIndicator.stopAnimating()
        loadingView.isHidden = true
    }
}

//MARK: - UITextFieldDelegate Implementation
extension LoginViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        normalizeField(textField: textField)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        normalizeField(textField: textField)
    }
    
    func normalizeField(textField: UITextField) {
        switch textField.tag {
        case 1:
            normalizeEmailField()
        case 2:
            normalizePasswordField()
        default:
            break
        }
    }
}

//MARK: - LoginViewModelDelegate Implementation
extension LoginViewController: LoginViewModelDelegate {
    func showLoginNotAllowed() {
        state = .presenting
        
        let alertController = UIAlertController(title: "Autenticação", message: "Login inválido, preencha as informações corretamente!", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Entendi", style: .default, handler: nil)
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }
}
