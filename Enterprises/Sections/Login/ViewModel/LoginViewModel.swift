//
//  LoginViewModel.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 08/04/22.
//

import Foundation

protocol LoginViewModelDelegate: AnyObject {
    func showLoginNotAllowed()
}

class LoginViewModel {
    
    //MARK: - Global Variables
    weak var delegate: LoginViewModelDelegate?
    var coordinator: LoginCoordinator?
    var service: LoginService?
    
    //MARK: - Initializer
    init(service: LoginService, coordinator: LoginCoordinator) {
        self.service = service
        self.coordinator = coordinator
    }
    
    func validateEmail(_ email: String) -> Bool {
        return ValidationHelper.validateEmail(email)
    }
    
    //MARK: - Public Functions
    func autenticate(login: Login) {
        
        service?.autenticateLogin(with: login, completion: { result in
            switch result {
            case .success(let authentication):
                AutheticationManager.shared.saveLocalAuthenticationContext(with: authentication)
                DispatchQueue.main.async {
                    self.coordinator?.flowToHome()
                }
            case .failure(let error):
                switch error {
                case .unauthorized:
                    fallthrough
                case .others:
                    DispatchQueue.main.async {
                        self.delegate?.showLoginNotAllowed()
                    }
                }
            }
        })
    }
}
