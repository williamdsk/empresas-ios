//
//  LoginService.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import Foundation

typealias loginCompletion = (Result<Authentication, LoginError>) -> ()

//Estou tratando apenas o caso 401, onde o login não está autorizado
//Porém a estrutura suporta crescimento
enum LoginError: Error {
    case unauthorized
    case others
}

class LoginService {
    
    func autenticateLogin(with login: Login, completion: @escaping loginCompletion) {
        
        let urlRequest = HTTPResquest.login(login: login).request
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) { data, urlResponse, error in
            
            if let _ = error {
                completion(.failure(.others))
            }
            
            guard let urlResponse = urlResponse as? HTTPURLResponse,
            let safeData = data,
                  let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: safeData)
            else {
                return
            }
            
            let statusCode = urlResponse.statusCode
            if statusCode == 200 && loginResponse.success
            {
                
                if let accessToken = urlResponse.value(forHTTPHeaderField: "access-token"),
                   let client = urlResponse.value(forHTTPHeaderField: "client"),
                   let uid = urlResponse.value(forHTTPHeaderField: "uid") {
                    
                    let authentication = Authentication(accessToken: accessToken, client: client, uid: uid)
                    completion(.success(authentication))
                    return
                }
                
            } else if statusCode == 401 {
                completion(.failure(.unauthorized))
                return
            }
            
            completion(.failure(.others))
        }
            
        task.resume()
    }
}
