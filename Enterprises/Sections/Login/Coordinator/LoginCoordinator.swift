//
//  LoginCoordinator.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 08/04/22.
//

import UIKit

class LoginCoordinator: Coordinator {
    //MARK: - Global Variables
    private(set) var navigationController: UINavigationController

    //MARK: - Initializer
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    //MARK: - Public Functions
    func start() {
        let loginService = LoginService()
        let loginViewModel = LoginViewModel(service: loginService, coordinator: self)
        let loginViewController = LoginViewController(viewModel: loginViewModel)
        
        navigationController.setViewControllers([loginViewController], animated: true)
    }
    
    func flowToHome() {
        let homeCoordinator = EnterpriseCoordinator(navigationController: navigationController)
        homeCoordinator.start()
    }
}
