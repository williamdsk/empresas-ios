//
//  HomeCoordinator.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import UIKit

class EnterpriseCoordinator {
    
    //MARK: - Global Variables
    private(set) var navigationController: UINavigationController
    private var homeViewModel: EnterpriseViewModel!

    //MARK: - Initializer
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    //MARK: - Public Functions
    func start() {
        let homeService = HomeService()
        homeViewModel = EnterpriseViewModel(service: homeService, coordinator: self)
        let homeViewController = HomeViewController(viewModel: homeViewModel)
        
        navigationController.setViewControllers([homeViewController], animated: true)
    }
    
    func flowToEnterprises(with keyword: String) {
        homeViewModel.keyword = keyword
        let enterprisesViewController = SearchViewController(viewModel: homeViewModel)
        navigationController.pushViewController(enterprisesViewController, animated: true)
    }
    
    func flowToDetails(with enterprise: Enterprise) {
        homeViewModel.enterpriseDetail = enterprise
        let detailViewcontroller = DetailsViewController(viewModel: homeViewModel)
        navigationController.pushViewController(detailViewcontroller, animated: true)
    }
}
