//
//  LoginService.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import Foundation

typealias homeCompletion = (Result<[Enterprise], EnterpriseError>) -> ()

//Estou tratando apenas o caso 401, onde o login não está autorizado
//Porém a estrutura suporta crescimento
enum EnterpriseError: Error {
    case unauthorized
    case noData
    case others
}

class HomeService {
    
    func fetchEnterprises(with keyword: String, completion: @escaping homeCompletion) {
        
        let urlRequest = HTTPResquest.home(name: keyword).request
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) { data, urlResponse, error in
            
            if let _ = error {
                completion(.failure(.others))
            }
            
            guard let urlResponse = urlResponse as? HTTPURLResponse,
                  let safeData = data,
                  let homeResponse = try? JSONDecoder().decode(EnterpriseResponse.self, from: safeData)
            else {
                completion(.failure(.others))
                return
            }
            
            let statusCode = urlResponse.statusCode
            if statusCode == 200
            {
                completion(.success(homeResponse.enterprises))
                return
            } else if statusCode == 401 {
                completion(.failure(.unauthorized))
                return
            }
            
            completion(.failure(.others))
        }
            
        task.resume()
    }
}
