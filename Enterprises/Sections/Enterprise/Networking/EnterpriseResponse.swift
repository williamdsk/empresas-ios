//
//  LoginResponse.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import Foundation

struct EnterpriseResponse: Codable {
    var enterprises: [Enterprise]
}
