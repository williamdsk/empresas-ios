//
//  HomeViewController.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var homeSearchBar: UISearchBar!
    @IBOutlet weak var searchStackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Global Variable
    var viewModel: EnterpriseViewModel!

    // MARK: - Initializers
    init(viewModel: EnterpriseViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
        setupDelegates()
    }
    
    // MARK: - UI Configuration
    private func configUI() {
        navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.titleView = nil
        
        homeSearchBar.searchTextField.enablesReturnKeyAutomatically = true
        homeSearchBar.searchTextField.backgroundColor = .white
        homeSearchBar.searchTextField.textColor = Color.darkText
        
        searchStackView.layer.borderColor = UIColor.gray.cgColor
        searchStackView.layer.borderWidth = 1
        searchStackView.layer.cornerRadius = 10
        
        titleLabel.text = """
        Pesquise por
        uma empresa
        """
    }
    
    // MARK: - Private Functions
    private func setupDelegates() {
        homeSearchBar.searchTextField.delegate = self
    }
    
    private func showInvalidSearchInput() {
        let alertController = UIAlertController(title: "Aviso", message: "Informe o nome de uma empresa para efetuar a busca!", preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Entendi", style: .default, handler: nil)
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UITextFieldDelegate Implementation
extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        if let keyword = textField.text, !textField.text!.isEmpty {
            viewModel.search(with: keyword)
            
            return true
        }
        
        showInvalidSearchInput()
        return true
    }
}

