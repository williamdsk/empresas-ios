//
//  EnterpriseDetailViewController.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import UIKit
import Kingfisher

class DetailsViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var enterpriseImageView: UIImageView!
    @IBOutlet weak var enterpriseDescriptionLabel: UILabel!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    //MARK: - Global Variables
    var viewModel: EnterpriseViewModel!

    //MARK: - View Code Components
    lazy var backButton: UIBarButtonItem = {
        let backButton = UIBarButtonItem(image: UIImage(systemName: "arrow.left"), style: .plain, target: self, action: #selector(goBack))
        
        backButton.tintColor = Color.lightButtonTint
        
        return backButton
    }()
    
    //MARK: - Initializers
    init(viewModel: EnterpriseViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationItem.leftBarButtonItem = backButton
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fetchDetails()
    }
    //MARK: - UI Configuration
    private func configUI() {
        title = "Enterprise"
        
        startLoading()
    }
  
    //MARK: - Private Functions
    private func fetchDetails() {
        guard let enterpriseDetail = viewModel.enterpriseDetail else { return }
        
        navigationItem.setTitle(
            enterpriseDetail.enterpriseName,
            titleFont: Font.navigationTitle,
            subtitle: enterpriseDetail.enterpriseType.enterpriseTypeName,
            subtitleFont: Font.navigationSubtitle,
            textColor: .white
        )
           
        enterpriseImageView.kf.setImage(with: enterpriseDetail.getUrl(), placeholder: UIImage(systemName: "photo.artframe"))
        enterpriseImageView.roundCorners([.bottomLeft, .bottomRight], radius: 24)

        enterpriseDescriptionLabel.text = enterpriseDetail.getFormattedDescription()
        
        startPresenting()
    }
    
    //MARK: - Button Actions
    @objc private func goBack() {
        navigationController?.popViewController(animated: true)
    }
}

//MARK: - ViewController State Handler
extension DetailsViewController {
    func startLoading() {
        loadingActivityIndicator.startAnimating()
        loadingActivityIndicator.isHidden = false
        
        enterpriseDescriptionLabel.isHidden = true
        enterpriseImageView.isHidden = true
    }
    
    func startPresenting() {
        loadingActivityIndicator.stopAnimating()
        loadingActivityIndicator.isHidden = true
        
        enterpriseDescriptionLabel.isHidden = false
        enterpriseImageView.isHidden = false
    }
}
