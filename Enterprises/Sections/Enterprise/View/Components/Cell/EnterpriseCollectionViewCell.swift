//
//  EnterpriseCollectionViewCell.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import UIKit
import Kingfisher

class EnterpriseCollectionViewCell: UICollectionViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    //MARK: - Identifier Constant
    static let identifier: String = "EnterpriseCollectionViewCell"
    
    //MARK: - Life Cyle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configUI()
    }

    //MARK: - UI Configuration
    private func configUI() {
        backgroundImageView.layer.cornerRadius = 15
        backgroundImageView.clipsToBounds = true
        
        photoImageView.layer.cornerRadius = 15
        photoImageView.clipsToBounds = true
      
        mainView.layer.backgroundColor = UIColor.clear.cgColor
        mainView.layer.shadowColor = UIColor.black.cgColor
        mainView.layer.shadowOffset = CGSize(width: 0, height: 1.5)
        mainView.layer.shadowOpacity = 0.30
        mainView.layer.shadowRadius = 2.5
                
        containerView.layer.cornerRadius = 12
        containerView.layer.masksToBounds = true
    }
    
    //MARK: - Public Functions
    func setup(with enterprise: Enterprise) {
        let url = HTTPResquest.photo(path: enterprise.photo).url
        photoImageView.kf.setImage(with: url)
        
        nameLabel.text = enterprise.enterpriseName
    }
}
