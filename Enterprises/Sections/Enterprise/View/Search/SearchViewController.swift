//
//  EnterprisesViewController.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import UIKit

class SearchViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var searchStackView: UIStackView!
    @IBOutlet weak var notFoundImageView: UIImageView!
    @IBOutlet weak var enterprisesCollectionView: UICollectionView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var enterprisesSearchBar: UISearchBar!
    @IBOutlet weak var notFoundLabel: UILabel!
    
    //MARK: - Global Variables
    var viewModel: EnterpriseViewModel!
    
    var state: State = .loading{
        didSet {
            switch state {
            case .loading:
                self.startLoading()
            case .presenting:
                self.startPresenting()
            }
        }
    }
    
    //MARK: - UI Components
    lazy var backButton: UIBarButtonItem = {
        let backButton = UIBarButtonItem(image: UIImage(systemName: "arrow.left"), style: .plain, target: self, action: #selector(goBack))
        
        backButton.tintColor = .purple
        
        return backButton
    }()
    
    //MARK: - Initializer
    init(viewModel: EnterpriseViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        setupDelegates()
        setupDatasources()
        viewModel.showEnterprises()
    }
 
    //MARK: - UI Configuration
    private func configUI() {
        state = .loading
        
        title = "Pesquise"
        navigationController?.navigationBar.titleTextAttributes = [.font: Font.navigationTitle]
        navigationItem.leftBarButtonItem = backButton
        
        enterprisesSearchBar.searchTextField.enablesReturnKeyAutomatically = true
        enterprisesSearchBar.searchTextField.backgroundColor = .white
        
        searchStackView.layer.borderColor = Color.searchFieldBorder.cgColor
        searchStackView.layer.borderWidth = 1
        searchStackView.layer.cornerRadius = 10
        
        notFoundImageView.isHidden = true
        
        register()
        layout()
    }
    
    private func layout() {
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .vertical
        enterprisesCollectionView.showsVerticalScrollIndicator = false
        enterprisesCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    private func register() {
        let nib = UINib(nibName: EnterpriseCollectionViewCell.identifier, bundle: nil)
        
        enterprisesCollectionView.register(nib, forCellWithReuseIdentifier: EnterpriseCollectionViewCell.identifier)
    }
    
    private func setupDelegates() {
        viewModel.delegate = self
        enterprisesSearchBar.searchTextField.delegate = self
        enterprisesCollectionView.delegate = self
        
    }
    
    private func setupDatasources() {
        enterprisesCollectionView.dataSource = self
    }
    
    @objc private func goBack() {
        navigationController?.popViewController(animated: true)
    }
}

//MARK: - UICollectionViewDataSource Implementation
extension SearchViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.enterprises.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EnterpriseCollectionViewCell.identifier, for: indexPath) as? EnterpriseCollectionViewCell else { return UICollectionViewCell() }
        
        let enterprise = viewModel.enterprises[indexPath.row]
        
        cell.setup(with: enterprise)
        return cell
    }
}

//MARK: - UICollectionViewDelegate Implementation
extension SearchViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let enterprise = viewModel.enterprises[indexPath.row]
        viewModel.coordinator?.flowToDetails(with: enterprise)
    }
   
}

//MARK: - UICollectionViewDelegateFlowLayout Implementation
extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width * 0.47, height: collectionView.bounds.height * 0.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}

//MARK: - EnterpriseViewModelDelegate Implementation
extension SearchViewController: EnterpriseViewModelDelegate {
    
    func noResults() {
        state = .presenting
        
        enterprisesCollectionView.isHidden = true
        notFoundImageView.isHidden = false
        notFoundLabel.isHidden = false
    }
    
    func showResults() {
        state = .presenting
        
        notFoundImageView.isHidden = true
        notFoundLabel.isHidden = true
        enterprisesCollectionView.isHidden = false
        enterprisesCollectionView.reloadData()
    }
    
    func showError() {
        state = .presenting
        
        enterprisesCollectionView.isHidden = true
        notFoundImageView.isHidden = false
        notFoundLabel.isHidden = false
    }
}

//MARK: - UITextFieldDelegate Implementation
extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        if let keyword = textField.text, !textField.text!.isEmpty {
            state = .loading
            viewModel.fetchEnterprises(with: keyword)
        }

        return true
    }
}

//MARK: - LoadingActivityIndicator Handler
extension SearchViewController {
    func startLoading() {
        loadingActivityIndicator.startAnimating()
        loadingActivityIndicator.isHidden = false
        enterprisesCollectionView.isHidden = true
        notFoundImageView.isHidden = true
        notFoundLabel.isHidden = true
    }
    
    func startPresenting() {
        loadingActivityIndicator.stopAnimating()
        loadingActivityIndicator.isHidden = true
    }
}
