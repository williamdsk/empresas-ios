//
//  HomeViewModel.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import Foundation

protocol EnterpriseViewModelDelegate: AnyObject {
    func noResults()
    func showResults()
    func showError()
}

class EnterpriseViewModel {
    
    //MARK: - Global Variables
    weak var delegate: EnterpriseViewModelDelegate?
    var coordinator: EnterpriseCoordinator?
    var service: HomeService?
    var enterprises: [Enterprise] = []
    var enterpriseDetail: Enterprise?
    var keyword: String?
    
    //MARK: - Initializer
    init(service: HomeService, coordinator: EnterpriseCoordinator) {
        self.coordinator = coordinator
        self.service = service
    }
    
    //MARK: - Public Functions
    func search(with keyword: String) {
        self.keyword = keyword
        coordinator?.flowToEnterprises(with: keyword)
    }
    
    func showEnterprises() {
        if let keyword = keyword {
            fetchEnterprises(with: keyword)
        }
    }
    
    func fetchEnterprises(with keyword: String) {
        service?.fetchEnterprises(with: keyword, completion: { result in
            switch result {
                case .success(let enterprises):
                    if enterprises.isEmpty { 
                        DispatchQueue.main.async {
                            self.delegate?.noResults()
                        }
                        return
                    }
                    DispatchQueue.main.async {
                        self.enterprises = enterprises
                        self.delegate?.showResults()
                    }
                case .failure:
                DispatchQueue.main.async {
                    self.delegate?.showError()
                }
            }
        })
    }
    
    func showDetails(enterprise: Enterprise) {
        coordinator?.flowToDetails(with: enterprise)
    }
}
