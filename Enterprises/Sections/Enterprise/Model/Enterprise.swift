//
//  Enterprise.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import Foundation

struct Enterprise: Codable {
    let id: Int
    let enterpriseName, enterpriseDescription: String
    let photo: String
    let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id, photo
        case enterpriseName = "enterprise_name"
        case enterpriseDescription = "description"
        case enterpriseType = "enterprise_type"
    }
    
    func getFormattedDescription() -> String{
        return enterpriseDescription.components(separatedBy: "\n").reduce("\t") { partialResult, nextString in
            partialResult + "\n\t" + nextString
        }
    }
    
    func getUrl() -> URL {
        let url = HTTPResquest.photo(path: photo).url
        return url
    }
}

struct EnterpriseType: Codable {
    let id: Int
    let enterpriseTypeName: String

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
}
