//
//  AuthenticationManager.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import Foundation

class AutheticationManager {
    
    let keychainService: String = "oauth2.0"
    let keychainAccount: String = "ioasys"
    
    static var shared: AutheticationManager {
        let instance = AutheticationManager()
        
        return instance
    }
    
    func getLocalAuthenticationContext() -> Authentication {
        if let data = KeychainHelper.shared.read(service: keychainService, account: keychainAccount),
              let authentication = try? JSONDecoder().decode(Authentication.self, from: data) {
            
            return authentication
        }
        
        return Authentication(accessToken: "", client: "", uid: "")
    }
    
    func saveLocalAuthenticationContext(with authentication: Authentication) {
        let data = try! JSONEncoder().encode(authentication)
        KeychainHelper.shared.save(data, service: keychainService, account: keychainAccount)
    }
}

struct Authentication: Codable {
    let accessToken: String
    let client: String
    let uid: String
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access-token"
        case client, uid
    }
}
