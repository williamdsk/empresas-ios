//
//  HTTPRequest.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 09/04/22.
//

import Foundation

enum HTTPResquest {
    
    case login(login: Login)
    case home(name: String)
    case photo(path: String)
    
    var request: URLRequest {
        switch self {
        case .login:
            fallthrough
        case .home:
            fallthrough
        case .photo:
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = method
            urlRequest.httpBody = body
            
            headers.forEach {
                urlRequest.setValue($0, forHTTPHeaderField: $1)
            }
            
            authenticationHeaders.forEach {
                urlRequest.setValue($0, forHTTPHeaderField: $1)
            }
            
            return urlRequest
        }
    }
    
    private var authenticationHeaders: [(String, String)] {
        switch self {
        case .login:
            return []
        case .home:
            let authentication = AutheticationManager.shared.getLocalAuthenticationContext()
            
            return [
                (authentication.accessToken, "access-token"),
                (authentication.client, "client"),
                (authentication.uid, "uid")
            ]
        case .photo:
            return []
        }
    }
    
    var url: URL {
        switch self {
        case .login:
            fallthrough
        case .home:
            return URL(string: "\(baseUrl)/api/\(apiVersion)/\(path)")!
        case .photo:
            return URL(string: "\(baseUrl)/\(path)")!
        }
    }
    
    private var body: Data {
        switch self {
        case .login(let login):
            let body = try! JSONEncoder().encode(login)
            return body
        case .home:
            fallthrough
        case .photo:
            return Data()
        }
    }
    
    private var baseUrl: String {
        switch self {
        case .login:
            fallthrough
        case .home:
            fallthrough
        case .photo:
            return "https://empresas.ioasys.com.br"
        }
    }
    
    private var apiVersion: String {
        switch self {
        case .login:
            fallthrough
        case .home:
            return "v1"
        case .photo:
            return ""
        }
    }
    
    private var path: String {
        switch self {
        case .login:
            return "users/auth/sign_in"
        case .home(let name):
            return "enterprises?name=\(name.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)"
        case .photo(let path):
            return path
        }
    }
    
    private var method: String {
        switch self {
        case .login:
            return "POST"
        case .home:
            fallthrough
        case .photo:
            return "GET"
        }
    }
    
    private var headers: [(String, String)]  {
        switch self {
        case .login:
            fallthrough
        case .home:
            let headers = [
                ("application/json", "content-type")
            ]
            return headers

        case .photo:
            return []
        }
    }
}
