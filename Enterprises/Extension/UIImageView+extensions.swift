//
//  UIImageView+extensions.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import UIKit

extension UIImageView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds,  byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        
        self.layer.mask = mask
    }
}
