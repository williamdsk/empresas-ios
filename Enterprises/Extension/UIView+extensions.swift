//
//  UIView+extensions.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 10/04/22.
//

import UIKit

// Inspirado pela thread no stackoverflow https://stackoverflow.com/questions/38626004/add-subtitle-under-the-title-in-navigation-bar-controller-in-xcode
// Agradecimento aos usuário Dan -> https://stackoverflow.com/users/1714526/dan
extension UINavigationItem {

    func setTitle(_ title: String,
                  titleFont: UIFont,
                  subtitle: String,
                  subtitleFont: UIFont,
                  textColor: UIColor) {

        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = titleFont
        titleLabel.textColor = textColor

        let subtitleLabel = UILabel()
        subtitleLabel.text = subtitle
        subtitleLabel.font = subtitleFont
        subtitleLabel.textColor = textColor

        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.spacing = 15

        titleView = stackView
  }
}
