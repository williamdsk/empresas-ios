//
//  AppCoordinator.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 08/04/22.
//

import UIKit

class AppCoordinator: Coordinator {
        
    private(set) var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        flowToLogin()
    }
    
    func flowToLogin() {
        let loginCoordinator = LoginCoordinator(navigationController: navigationController)
        loginCoordinator.start()
    }
    
    func flowToHome() {
        
    }
}
