//
//  Coordinator.swift
//  Enterprises
//
//  Created by Idwall Go Dev 003 on 08/04/22.
//

import UIKit

protocol Coordinator {
        
    var navigationController: UINavigationController { get }
    
    func start()
    
}
